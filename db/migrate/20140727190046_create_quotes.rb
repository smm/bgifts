class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.string :quote_author
      t.text :quote_text

      t.timestamps
    end
  end
end
