class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text :link
      t.string :image_file_name
      t.decimal :price
      t.boolean :featured
      t.references :category, index: true

      t.timestamps
    end
  end
end
