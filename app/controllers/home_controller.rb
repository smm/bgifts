class HomeController < ApplicationController
  	
  	def index
    	@products = Product.all
	end

	#Category sorts

	def tools
		@products = Product.all.where( category_id: "1" )
	end

	def apparel
		@products = Product.all.where( category_id: "2" )
	end	

	def health
		@products = Product.all.where( category_id: "3" )
	end	

	def electronics
		@products = Product.all.where( category_id: "4" )
	end	

	#Price sorts

	def budget
		@products = Product.all.where( price: 0..100 )
	end

	def special
		@products = Product.all.where( price: 100..250 )
	end

	def luxury
		@products = Product.all.where( price: 250..100000 )
	end
	
	#Blog
	def blog
		#@blogs = Product.all.where( category_id: "4" )
	end	
end